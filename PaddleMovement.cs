﻿using UnityEngine;
using System.Collections;

public class PaddleMovement : MonoBehaviour {

    private SpriteRenderer sRenderer;

	// Use this for initialization
	void Start () {
        sRenderer = GetComponent<SpriteRenderer>();

    }
	
	// Update is called once per frame
	void Update () {
        //print(16 * Input.mousePosition.x / Screen.width - 8);
        float anchoPaleta = sRenderer.bounds.size.x;
        transform.position = new Vector3(
            Mathf.Clamp(16 * Input.mousePosition.x / Screen.width - 8F, -8F + anchoPaleta/2, 8F - anchoPaleta/2),
            transform.position.y, 
            transform.position.z
        );
    }
}
