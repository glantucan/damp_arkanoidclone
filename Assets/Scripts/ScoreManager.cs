﻿using UnityEngine;
using System.Collections;

public class ScoreManager : APersistant {

    public int playTime; // in seconds

	override protected void Start () {
        base.Start();
	}
    
    override protected void Update () {
        base.Start();
        playTime = (int) Time.time;
    }
}
