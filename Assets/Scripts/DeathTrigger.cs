﻿using UnityEngine;
using System.Collections;

public class DeathTrigger : MonoBehaviour {

	private LevelManager lvlMgr;
	
	// Use this for initialization
	void Start () {
		lvlMgr = GameObject.FindObjectOfType<LevelManager>();
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {
		lvlMgr.LoadLevel("Start Menu");
	}
}
