using UnityEngine;
using System.Collections;

public interface ISoundManager 
{
	AudioClip soundToPlay();
}

