﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    private int brickCount;
	private bool _levelFinished;
	public bool levelFinished { get{ return _levelFinished;} }
    public void addBrick() {
        brickCount++;
    }

    public void substractBrick() {
        brickCount--;
        if (brickCount == 0) { 
            passLevel();
        }
    }

    private void passLevel() {
        print("Level " + Application.loadedLevel + " passed");
        Invoke("loadNextLevel", 3);
    }

    private void loadNextLevel() {
    	_levelFinished = true;
        Application.LoadLevel(Application.loadedLevel + 1);
    }

	public void LoadLevel(string name) {
		_levelFinished = true;
		Application.LoadLevel (name);
	}


	public void QuitRequest() {
		Debug.Log ("Quit requested");
		Application.Quit ();
	}


}
