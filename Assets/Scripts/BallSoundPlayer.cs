﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class BallSoundPlayer : MonoBehaviour {
    [SerializeField]
    private AudioClip[] sounds;
    private AudioSource source;
    
	protected void Start () {
        source = GetComponent<AudioSource>();
	}
	
	
	
    private void OnCollisionEnter2D(Collision2D col) {
		ISoundManager otherSoundManager = col.collider.GetComponent<ISoundManager>();
		if (otherSoundManager == null) {
            
            source.clip = sounds[whichSound()];
            
        } else {
        	source.clip = otherSoundManager.soundToPlay();
        }
		source.Play();
    }
    
	virtual protected int whichSound() {
		int index;
		if (sounds.Length > 1) {
			index = Random.Range(0, sounds.Length);
		} else {
			index = 0;
		}
		return index;
	}
}
