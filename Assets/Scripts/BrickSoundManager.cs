﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BrickResistance))]
public class BrickSoundManager : MonoBehaviour,  ISoundManager {

    private BrickResistance brickRes;
	[SerializeField]
	private AudioClip[] sounds;
	
    void Start() {
        brickRes = GetComponent<BrickResistance>();
    }

    public AudioClip soundToPlay() {
    	AudioClip chosenSound;
		if (brickRes.hitsCount - 1 < sounds.Length) {
			chosenSound = sounds[brickRes.hitsCount - 1];
		} else {
			chosenSound = null;
		}
		return chosenSound;
    }
}
