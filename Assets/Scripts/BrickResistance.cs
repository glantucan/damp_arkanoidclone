﻿using UnityEngine;
using System.Collections;

public class BrickResistance : MonoBehaviour {
    [SerializeField]
    private int _maxHits;
    public int maxHits {
        get { return _maxHits; }
    }

    public Sprite[] hitSprites;
    public bool unbreakable;


    private int _hitsCount;
    public int hitsCount {
        get { return _hitsCount; }
    }

    private LevelManager lvlMgr;
	private SpriteRenderer sRenderer;
	
	
	
	void Start() {
		if (!unbreakable) {
			lvlMgr = GameObject.FindObjectOfType<LevelManager>();
			lvlMgr.addBrick();
            sRenderer = GetComponent<SpriteRenderer>();
		}
	}
	
	void OnCollisionEnter2D (Collision2D col) {
		if (!unbreakable) {
			_hitsCount++;
			if (_hitsCount >= _maxHits) {
				lvlMgr.substractBrick();
				Destroy(gameObject);
			} else {
				loadSprites();
			}
		}
	}
	
	void loadSprites() {
		int spriteIndex = hitsCount-1;
        sRenderer.sprite = hitSprites[spriteIndex];
	}
}