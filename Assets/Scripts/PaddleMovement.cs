﻿using UnityEngine;
using System.Collections;

public class PaddleMovement : MonoBehaviour {
	
	
	private float width;
	
	// Use this for initialization
	void Start () {
		width = GetComponent<Renderer>().bounds.size.x;
       
	}
	
	// Update is called once per frame
	void Update () {
        //print(16 * Input.mousePosition.x / Screen.width - 8);
        transform.position = new Vector3(
            Mathf.Clamp(16 * Input.mousePosition.x / Screen.width - 8F, -8F + width/2, 8F - width/2),
            transform.position.y, 
            transform.position.z
        );
        
        
    }
}
