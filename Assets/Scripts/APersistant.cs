﻿using System;
using UnityEngine;
using System.Collections;

public class APersistant : MonoBehaviour {

    private float _lifeTime;
    public float lifeTime {
        get { return _lifeTime; }
    }
    
    virtual protected void Start () {
        Type managerType = this.GetType();
        _lifeTime = 0f;
        APersistant[] persistantObjects = FindObjectsOfType<APersistant>();
        GameObject.DontDestroyOnLoad(gameObject);
        if (persistantObjects.Length == 1) {
            GameObject.DontDestroyOnLoad(gameObject);
        }
        else {
            foreach (APersistant curObject in persistantObjects) {
                if (curObject.GetType() == managerType) {
                    if (curObject != this &&
                        curObject.lifeTime == this._lifeTime) {
                        curObject.beDestroyed();
                    }
                    else if (curObject.lifeTime > this._lifeTime) {
                        this.beDestroyed();
                    }
                }
            }
        }
    }
	
	virtual protected void Update () {
        _lifeTime += Time.deltaTime;
    }

    public void beDestroyed() {
        GameObject.Destroy(gameObject);
    }
}
