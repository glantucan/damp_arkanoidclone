﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {


    public bool ballFree;
    private Rigidbody2D rb;
    private Transform paddleTr;
    private Vector3 initialPaddleToBall;

    public float vel;

    // Use this for initialization
	void Start () {
        ballFree = false;
        // Convertir el rigidbody en cinemático
        rb = GetComponent<Rigidbody2D>();
        
        rb.isKinematic = true;
        PaddleMovement paddle = FindObjectOfType<PaddleMovement>();
        paddleTr = paddle.transform;
        initialPaddleToBall = transform.position - paddleTr.position;
    }
	
	// Update is called once per frame
	void Update () {
	    // Si ballFree false
        if (!ballFree) {
            rb.velocity = Vector2.zero;
            // posicion x igual a posicion x de paleta
            transform.position = new Vector3(paddleTr.position.x, transform.position.y, transform.position.z);
            if (Input.GetMouseButtonDown(0)) {
                ballFree = true;
                rb.isKinematic = false;
                rb.velocity = rb.velocity = new Vector2(Random.Range(-vel, vel), vel);
            }
        }
           
	}
}
