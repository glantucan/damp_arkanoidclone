﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BrickScore : MonoBehaviour {
	
	[SerializeField]
	private int scorePoints;
	private Text scoreField;
	private LevelManager lvlManager;
	
	void Start () {
		scoreField = GameObject.Find("/HUD/Score").GetComponent<Text>();
		lvlManager = FindObjectOfType<LevelManager>();
	}
	
	void OnDestroy () {
		if(!lvlManager.levelFinished) {
			int currentScore = int.Parse(scoreField.text);
			currentScore += scorePoints;
			scoreField.text = currentScore.ToString(); 
		}
	}
	
}
